package org.easyhotel.presentation;

import java.util.List;

import org.easyhotel.dao.HotelDAO;
import org.easyhotel.dao.HotelDAO_Hibernate;
import org.easyhotel.models.Hotel;

public class Hotels {
private String name;
private HotelDAO hoteldao = new HotelDAO_Hibernate();
private List<Hotel>  hotels ;
	
	public String execute(){
		hotels = hoteldao.getHotelsByName(name);
		return "succes";
	}

	public List<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(List<Hotel> hotels) {
		this.hotels = hotels;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
