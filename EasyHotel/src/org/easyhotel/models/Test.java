package org.easyhotel.models;

import java.util.List;

import org.easyhotel.dao.HotelDAO_Hibernate;

public class Test {

	
	public static void main(String[] args){
		
		HotelDAO_Hibernate reservationServices = new HotelDAO_Hibernate();
		
		List<Hotel> hotels = reservationServices.getHotels();
		
		if(hotels != null){
			for(int i=0;i<hotels.size();i++){
				Hotel hotel = (Hotel)hotels.get(i);
				System.out.println(hotel.getName());
			}
		}
	}
}
