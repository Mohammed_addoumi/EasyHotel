package org.easyhotel.models;

import java.util.Date;

public class Reservation {
	private Date checkIn_date;
	private Date checkOut_date;
	private Date reservation_date;
	private int rooms_number;
	private float total_price;
	private int persons_number;
	
	Client client;
	Hotel hotel;
	
	
	public Reservation() {
		super();
	}
	public Date getCheckIn_date() {
		return checkIn_date;
	}
	public void setCheckIn_date(Date checkIn_date) {
		this.checkIn_date = checkIn_date;
	}
	public Date getCheckOut_date() {
		return checkOut_date;
	}
	public void setCheckOut_date(Date checkOut_date) {
		this.checkOut_date = checkOut_date;
	}
	public Date getReservation_date() {
		return reservation_date;
	}
	public void setReservation_date(Date reservation_date) {
		this.reservation_date = reservation_date;
	}
	public int getRooms_number() {
		return rooms_number;
	}
	public void setRooms_number(int rooms_number) {
		this.rooms_number = rooms_number;
	}
	public float getTotal_price() {
		return total_price;
	}
	public void setTotal_price(float total_price) {
		this.total_price = total_price;
	}
	public int getPersons_number() {
		return persons_number;
	}
	public void setPersons_number(int persons_number) {
		this.persons_number = persons_number;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
	
	

}
