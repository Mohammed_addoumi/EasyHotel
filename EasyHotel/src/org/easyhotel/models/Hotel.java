package org.easyhotel.models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="HOTEL")

public class Hotel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hotel_id")
	private int hotel_id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="room_total")
	private int room_total;
	
	@Column(name="booked_room_total")
	private int booked_room_total;
	
	@Column(name="owner")
	private String owner;
	
	@Column(name="description")
	private String description;
	
	@Column(name="rating")
	private int rating;
	
	@Column(name="room_price")
	private double room_price;
	
    @Embedded
	private Adresse adresse;
	
	public Hotel() {
		super();
	}

	public Hotel(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRoom_total() {
		return room_total;
	}

	public void setRoom_total(int room_total) {
		this.room_total = room_total;
	}

	public int getBooked_room_total() {
		return booked_room_total;
	}

	public void setBooked_room_total(int booked_room_total) {
		this.booked_room_total = booked_room_total;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getRoom_price() {
		return room_price;
	}

	public void setRoom_price(double room_price) {
		this.room_price = room_price;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	
	
	
	
	
	
	
	
}
