package org.easyhotel.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class Adresse {

	@Column(name="city")
	private String city;
	
	@Column(name="street")
	private String street;
	
	@Column(name="number")
	private int number;
	
	public Adresse() {
		super();
	}

	public Adresse(String city, String street, int number) {
		super();
		this.city = city;
		this.street = street;
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
	
	
}
