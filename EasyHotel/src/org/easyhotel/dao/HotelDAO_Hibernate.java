package org.easyhotel.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.easyhotel.models.Hotel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


/*this class implements HotelDAO and the presents the implementation 
using Hibernate Framework*/
public class HotelDAO_Hibernate implements HotelDAO{
	
	public HotelDAO_Hibernate(){}
	
	
	
	//get all the hotels from Database
	
	public List<Hotel> getHotels(){
		
		
		List<Hotel> hotelsList = new ArrayList<>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction Tx = session.beginTransaction();
		
		
		
		List Hotels = session.createQuery("from Hotel h").list();
		
		//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
				
				
				
	}
	


	
	
	//get all the hotels from Database that has the name equals to the argument
	
	@Override
	public List<Hotel> getHotelsByName(String name) {
		//the collction that will hold the list of hotels
				List<Hotel> hotelsList = new ArrayList<>();
				
				//open the session 
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				//begin the transaction
				Transaction Tx = session.beginTransaction();
				
				
				
				//the query
		
		Query query= session.createQuery("from Hotel h where h.name='"+name+"'");
		
		List Hotels = query.list();
		//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
	}

	
	

	//get all the hotels from Database whose room price is between prixMin and prixMax
	@Override
	public List<Hotel> getHotels(double prixMin, double prixMax) {
		//the collction that will hold the list of hotels
				List<Hotel> hotelsList = new ArrayList<>();
				
				//open the session 
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				//begin the transaction
				Transaction Tx = session.beginTransaction();
				
				
				
				//the query
		List Hotels = session.createQuery("from Hotel h where h.room_price >= "+prixMin+"and h.room_price <='"+prixMax+"'").list();
		
		//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
	}


	//get all the hotels from Database whose rating is rating in the method argument
	
	@Override
	public List<Hotel> getHotelsByRating(int rating) {
		//the collction that will hold the list of hotels
				List<Hotel> hotelsList = new ArrayList<>();
				
				//open the session 
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				//begin the transaction
				Transaction Tx = session.beginTransaction();
				
				
				
				//the query
		List Hotels = session.createQuery("from Hotel h where h.rating = '"+rating+"'").list();
		
				//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
	}

	
	
	//get all the hotels from Database that are in the same city passed as argument
	@Override
	public List<Hotel> getHotelsByCity(String city) {
		//the collction that will hold the list of hotels
		List<Hotel> hotelsList = new ArrayList<>();
		
		//open the session 
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		//begin the transaction
		Transaction Tx = session.beginTransaction();
		
		
		
		//the query 
		List Hotels = session.createQuery("from Hotel h where h.adresse.city = '"+city+"'").list();
		
				//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
	}


	
	//get all the hotels from Database that match all the constraint passed as arguments
	@Override
	public List<Hotel> getHotels(String city, int rating, double prixMin, double prixMax) {
		//the collction that will hold the list of hotels
				List<Hotel> hotelsList = new ArrayList<>();
				
				//open the session 
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				//begin the transaction
				Transaction Tx = session.beginTransaction();
				
				
				
				//the query
		List Hotels = session.createQuery("from Hotel h where h.adresse.city = '"+city
				+"' and h.rating = "+rating+" and h.room_price >="+prixMin
				+" and h.room_price <= '"+prixMax+"'").list();
		
		//iterate through the result of the query
				for(Iterator i = Hotels.iterator();i.hasNext();){
					//construct the hotel object
					Hotel hotel=(Hotel)i.next();
				    
					//add the hotel to the list
					hotelsList.add(hotel);
				}
				
				//finish the transaction 
				Tx.commit();
				//close the session
				session.close();
				
				
				//check if there is hotels 
				if(hotelsList.size() !=0){
					return hotelsList;
				}
				else{
					return null;
				}
	}


	
	//add the hotel added by the Administrator to the Database
	@Override
	public void saveHotel(Hotel hotel) {
		// TODO Auto-generated method stub
		
	}
	

}
