package org.easyhotel.dao;

import java.util.List;

import org.easyhotel.models.Hotel;

public interface HotelDAO {

	public List<Hotel> getHotels();
	public List<Hotel> getHotelsByName(String name);
	public List<Hotel> getHotels(double prixMin,double prixMax);
	public List<Hotel> getHotelsByRating(int rating);
	public List<Hotel> getHotelsByCity(String city);
	public List<Hotel> getHotels(String city,int rating,double prixMin,double prixMax);
	public void saveHotel(Hotel hotel);
	
}
