package org.easyhotel.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;


public class HibernateUtil {


	
	private static  SessionFactory sessionFactory;
	
	
	static{
		
		try{
			Configuration conf=new Configuration().configure();
			sessionFactory = conf.buildSessionFactory();
			
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("erreur");
		}
	}
	
	public static SessionFactory getSessionFactory(){
		
		return sessionFactory;
	}
	
	public static void shutdown(){
		getSessionFactory().close();
		
	}
}
