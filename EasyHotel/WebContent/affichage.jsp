<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Il y a <s:property value="hotels.size"/> hotels</h1>
<s:iterator value="hotels">
<h6><s:property value="name"/>,<s:property value="room_total"/>,<s:property value="booked_room_total"/>,<s:property value="owner"/>,<s:property value="description"/>,<s:property value="room_price"/>,<s:property value="adresse.city"/>,<s:property value="adresse.street"/>,<s:property value="adresse.number"/>,</h6>
</s:iterator>

</body>
</html>